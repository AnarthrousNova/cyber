import turtle

def star():
	t = turtle.Turtle()
	a = 0
	t.width(6)
	for a in range(0,7):
		if a == 0:
			t.pencolor("#520A9D")
		if a == 1:
			t.pencolor("#BC13EA")
		if a == 2:
			t.pencolor("#182BD9")
		if a == 3:
			t.pencolor("#22A827")
		if a == 4:
			t.pencolor("#E8F330")
		if a == 5:
			t.pencolor("#FFC530")
		if a == 6:
			t.pencolor("#DA330B")
		t.penup()
		t.rt(15)
		t.pendown()
		t.fd(100)
		t.lt(30)
		t.fd(100)
		t.rt(30)
		t.bk(100)
		t.lt(30)
		t.bk(100)
		t.lt(15)
		a = a + 1
		
	
	
	#turtle.tracer(0, 0)
	
# main function
def main():
	screen = turtle.Screen()
	star()

	screen.exitonclick()	
	

if __name__ == "__main__":
	main()
